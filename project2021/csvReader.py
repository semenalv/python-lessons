import csv

TESTY_DEN = 0
TESTY_CELKEM = 1
PRVNI_TESTY_DEN = 2
CELKEM_PRVNI_TESTU = 3

KUMUL_POCET_NAK = 4
KUMUL_POCET_VYLEC = 5
KUMUL_POCET_SMRT = 6

POCET_NOVYCH_NAKAZENYCH = 7
POCET_NOVYCH_UMRITI = 8
POCET_NOVYCH_UZDRAVENI = 9


class csvReader:

    file_one_name = ""
    file_two_name = ""
    statistics_dictionary = {}

    #standartni CSV reader
    #precte CSV soubory a da to do slovniku



    def __init__(self, file_one_name, file_two_name):
        self.file_one_name = file_one_name
        self.file_two_name = file_two_name

    def read_file_one(self):
        #otevrime 1. soubor
        with open(self.file_one_name, newline='') as stat_one_file:
            our_reader = csv.reader(stat_one_file, delimiter=' ', quotechar='|')
            i = 0

            #prochazime skrz vsechny radky souboru
            for data_line in our_reader:

                # prvni radek csv jsou nazvy, to nepotrebujeme
                if (i == 0):
                    i+=1
                    continue

                #rozdelime radek s info pres ","
                data_array = data_line[0].split(",")

                #prvni prvek je datum, to bude klic slovniku
                datum = data_array[0]

                #datum uz nepotrebujeme jako data
                del data_array[0]

                #prochazime skrz data array
                for j in range(len(data_array)):
                    #pokud data nemame, dame 0
                    if (data_array[j] == ''):
                        data_array[j] = 0
                        continue

                    #data mame jako string, ale musi to byt cislo tipu integer
                    data_array[j] = int(data_array[j])

                #dame to do slovniku
                self.statistics_dictionary[datum] = data_array



    def read_file_two(self):
        # otevrime 2. soubor
        with open(self.file_two_name, newline='') as stat_two_file:

            our_reader = csv.reader(stat_two_file, delimiter=' ', quotechar='|')
            i = 0

            # prochazime skrz vsechny radky souboru
            for data_line in our_reader:

                # prvni radek csv jsou nazvy, to nepotrebujeme
                if (i == 0):
                    i += 1
                    continue

                # rozdelime radek s info pres ","
                data_array = data_line[0].split(",")

                # prvni prvek je datum, to bude klic slovniku
                datum = data_array[0]

                # datum uz nepotrebujeme jako data
                del data_array[0]
                #taky nepotrebna data
                del data_array[3]

                # prochazime skrz data array
                for j in range(len(data_array)):
                    # pokud data nemame, dame 0
                    if (data_array[j] == ''):
                        data_array[j] = 0
                        continue
                    # data mame jako string, ale musi to byt cislo tipu integer
                    data_array[j] = int(data_array[j])

                # pridame to do existujiciho slovniku
                self.statistics_dictionary[datum] = self.statistics_dictionary[datum] + data_array + [0, 0, 0]