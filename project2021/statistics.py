from csvReader import *

import numpy as np
import matplotlib.pyplot as plt

TESTY_DEN = 0
TESTY_CELKEM = 1
PRVNI_TESTY_DEN = 2
CELKEM_PRVNI_TESTU = 3

KUMUL_POCET_NAK = 4
KUMUL_POCET_VYLEC = 5
KUMUL_POCET_SMRT = 6

POCET_NOVYCH_NAKAZENYCH = 7
POCET_NOVYCH_UMRITI = 8
POCET_NOVYCH_UZDRAVENI = 9

class statistics:

    dict_of_new_infected = {}
    dict_of_new_deaths = {}
    dict_of_new_healthy = {}

    rt_dict = {}

    max_rt = 0
    max_smrti = 0
    tragicky_den = ""

    uspech_leceni = {}
    reader = None

    def __init__(self):
        #vytvorime reader
        self.reader = csvReader("data/tests_stat.csv", "data/total_stat.csv")

    def init_reader(self):
        #reader precte oba soubory
        self.reader.read_file_one()
        self.reader.read_file_two()

        #statistics spocita pocty novych pripadu, umriti a uzdraveni
        self.count_pocet_novych_nakazenych()
        self.count_pocet_novych_umriti()
        self.count_pocet_novych_uzdraveni()

        #statistics spocita koeficient a uspech leceni
        self.count_rt()
        self.count_uspech_leceni()

        #statistics vykresli grafy
        self.plot_all()

    def count_pocet_novych_nakazenych(self):
        # Prevadime hodnoty slovniku na list
        list_of_values = list(self.reader.statistics_dictionary.values())
        i = 0

        # Prochazime skrz vsechny klice
        for data_line in self.reader.statistics_dictionary.keys():

            # u prvniho dne dame 0
            if (i == 0):
                self.reader.statistics_dictionary[data_line][POCET_NOVYCH_NAKAZENYCH] = 0
                self.dict_of_new_infected[i] = 0
                i = 1
                continue

            # nova hodnota je kumulativni pocet ve zkoumany den minus kumulativni pocet v den predchozi
            self.reader.statistics_dictionary[data_line][POCET_NOVYCH_NAKAZENYCH] = \
                list_of_values[i][KUMUL_POCET_NAK] - list_of_values[i-1][KUMUL_POCET_NAK]
            self.dict_of_new_infected[i] = self.reader.statistics_dictionary[data_line][POCET_NOVYCH_NAKAZENYCH]
            i += 1


    def count_pocet_novych_umriti(self):
        # Prevadime hodnoty slovniku na list
        list_of_values = list(self.reader.statistics_dictionary.values())
        i = 0

        # Prochazime skrz vsechny klice
        for data_line in self.reader.statistics_dictionary.keys():

            # u prvniho dne dame 0
            if (i == 0):
                self.reader.statistics_dictionary[data_line][POCET_NOVYCH_UMRITI] = 0
                self.dict_of_new_deaths[i] = 0
                i = 1
                continue

            # nova hodnota je kumulativni pocet ve zkoumany den minus kumulativni pocet v den predchozi
            self.reader.statistics_dictionary[data_line][POCET_NOVYCH_UMRITI] = \
                list_of_values[i][KUMUL_POCET_SMRT] - list_of_values[i-1][KUMUL_POCET_SMRT]
            self.dict_of_new_deaths[i] = self.reader.statistics_dictionary[data_line][POCET_NOVYCH_UMRITI]

            #zaroven hledame maximalni pocet smrti
            if (self.reader.statistics_dictionary[data_line][POCET_NOVYCH_UMRITI] > self.max_smrti):
                self.max_smrti = self.reader.statistics_dictionary[data_line][POCET_NOVYCH_UMRITI]
                self.tragicky_den = data_line

            i += 1


    def count_pocet_novych_uzdraveni(self):
        # Prevadime hodnoty slovniku na list
        list_of_values = list(self.reader.statistics_dictionary.values())
        i = 0

        # Prochazime skrz vsechny klice
        for data_line in self.reader.statistics_dictionary.keys():

            #u prvniho dne dame 0
            if (i == 0):
                self.reader.statistics_dictionary[data_line][POCET_NOVYCH_UZDRAVENI] = 0
                self.dict_of_new_healthy[i] = 0
                i = 1
                continue

            #nova hodnota je kumulativni pocet ve zkoumany den minus kumulativni pocet v den predchozi
            self.reader.statistics_dictionary[data_line][POCET_NOVYCH_UZDRAVENI] = \
                list_of_values[i][KUMUL_POCET_VYLEC] - list_of_values[i-1][KUMUL_POCET_VYLEC]
            self.dict_of_new_healthy[i] = self.reader.statistics_dictionary[data_line][POCET_NOVYCH_UZDRAVENI]
            i += 1

    def count_rt(self):
        #Prevadime hodnoty slovniku na list
        list_of_values = list(self.reader.statistics_dictionary.values())
        i = 0

        #Prochazime skrz vsechny klice
        for data_line in self.reader.statistics_dictionary.keys():
            self.rt_dict[data_line] = 0
            #pokud je to den drive nez 8. v statistice, nemuzeme spocitat
            if (i < 8):
                i += 1
                continue

            #kontrola deleni 0
            if (list_of_values[i-7][POCET_NOVYCH_NAKAZENYCH] != 0):
                self.rt_dict[data_line] = \
                    (list_of_values[i][POCET_NOVYCH_NAKAZENYCH] + list_of_values[i-1][POCET_NOVYCH_NAKAZENYCH] + list_of_values[i-2][POCET_NOVYCH_NAKAZENYCH] + list_of_values[i-3][POCET_NOVYCH_NAKAZENYCH])/(list_of_values[i-4][POCET_NOVYCH_NAKAZENYCH] + list_of_values[i-5][POCET_NOVYCH_NAKAZENYCH] + list_of_values[i-6][POCET_NOVYCH_NAKAZENYCH] + list_of_values[i-7][POCET_NOVYCH_NAKAZENYCH])

            #zaroven hledame maximalni koef.
            if (self.rt_dict[data_line] > self.max_rt):
                self.max_rt = self.rt_dict[data_line]
            i += 1

    def count_uspech_leceni(self):
        #Vypocet:
        #Pocet vylecenych behem mesice lomeno poctem novych nakazenych
        #(data za posledni den mesice minus data za posledni den minuleho mesice (krome brezna))

        self.uspech_leceni["brezen"] = \
            self.reader.statistics_dictionary["2020-03-31"][KUMUL_POCET_VYLEC] / \
            self.reader.statistics_dictionary["2020-03-31"][KUMUL_POCET_NAK]

        self.uspech_leceni["duben"] = \
            (self.reader.statistics_dictionary["2020-04-30"][KUMUL_POCET_VYLEC] - self.reader.statistics_dictionary["2020-03-31"][KUMUL_POCET_VYLEC]) / \
            (self.reader.statistics_dictionary["2020-04-30"][KUMUL_POCET_NAK] - self.reader.statistics_dictionary["2020-03-31"][KUMUL_POCET_NAK])

        self.uspech_leceni["kveten"] = \
            (self.reader.statistics_dictionary["2020-05-31"][KUMUL_POCET_VYLEC] -
             self.reader.statistics_dictionary["2020-04-30"][KUMUL_POCET_VYLEC]) / \
            (self.reader.statistics_dictionary["2020-05-31"][KUMUL_POCET_NAK] -
             self.reader.statistics_dictionary["2020-04-30"][KUMUL_POCET_NAK])

        self.uspech_leceni["cerven"] = \
            (self.reader.statistics_dictionary["2020-06-30"][KUMUL_POCET_VYLEC] -
             self.reader.statistics_dictionary["2020-05-31"][KUMUL_POCET_VYLEC]) / \
            (self.reader.statistics_dictionary["2020-06-30"][KUMUL_POCET_NAK] -
             self.reader.statistics_dictionary["2020-05-31"][KUMUL_POCET_NAK])

        self.uspech_leceni["cervenec"] = \
            (self.reader.statistics_dictionary["2020-07-31"][KUMUL_POCET_VYLEC] -
             self.reader.statistics_dictionary["2020-06-30"][KUMUL_POCET_VYLEC]) / \
            (self.reader.statistics_dictionary["2020-07-31"][KUMUL_POCET_NAK] -
             self.reader.statistics_dictionary["2020-06-30"][KUMUL_POCET_NAK])

        self.uspech_leceni["srpen"] = \
            (self.reader.statistics_dictionary["2020-08-31"][KUMUL_POCET_VYLEC] -
             self.reader.statistics_dictionary["2020-07-31"][KUMUL_POCET_VYLEC]) / \
            (self.reader.statistics_dictionary["2020-08-31"][KUMUL_POCET_NAK] -
             self.reader.statistics_dictionary["2020-07-31"][KUMUL_POCET_NAK])

        self.uspech_leceni["zari"] = \
            (self.reader.statistics_dictionary["2020-09-30"][KUMUL_POCET_VYLEC] -
             self.reader.statistics_dictionary["2020-08-31"][KUMUL_POCET_VYLEC]) / \
            (self.reader.statistics_dictionary["2020-09-30"][KUMUL_POCET_NAK] -
             self.reader.statistics_dictionary["2020-08-31"][KUMUL_POCET_NAK])

        self.uspech_leceni["rijen"] = \
            (self.reader.statistics_dictionary["2020-10-31"][KUMUL_POCET_VYLEC] -
             self.reader.statistics_dictionary["2020-09-30"][KUMUL_POCET_VYLEC]) / \
            (self.reader.statistics_dictionary["2020-10-31"][KUMUL_POCET_NAK] -
             self.reader.statistics_dictionary["2020-09-30"][KUMUL_POCET_NAK])

        self.uspech_leceni["listopad"] = \
            (self.reader.statistics_dictionary["2020-11-30"][KUMUL_POCET_VYLEC] -
             self.reader.statistics_dictionary["2020-10-31"][KUMUL_POCET_VYLEC]) / \
            (self.reader.statistics_dictionary["2020-11-30"][KUMUL_POCET_NAK] -
             self.reader.statistics_dictionary["2020-10-31"][KUMUL_POCET_NAK])

        self.uspech_leceni["prosinec"] = \
            (self.reader.statistics_dictionary["2020-12-31"][KUMUL_POCET_VYLEC] -
             self.reader.statistics_dictionary["2020-11-30"][KUMUL_POCET_VYLEC]) / \
            (self.reader.statistics_dictionary["2020-12-31"][KUMUL_POCET_NAK] -
             self.reader.statistics_dictionary["2020-11-30"][KUMUL_POCET_NAK])

    def plot_all(self):
        # vytvarime sadu mensich obrazku
        fig, axs = plt.subplots(3, 2, figsize=(12, 14), tight_layout=True)

        #graf pro nove pripady
        axs[0, 0].plot(range(0, self.dict_of_new_infected.keys().__len__()), self.dict_of_new_infected.values())
        axs[0, 0].set(xlabel='Dny od pocatku', ylabel='Pripady')
        axs[0, 0].set_title("Nove pripady")

        #graf pro smrti
        axs[0, 1].plot(range(0, self.dict_of_new_deaths.keys().__len__()), self.dict_of_new_deaths.values())
        axs[0, 1].set(xlabel='Dny od pocatku', ylabel='Smrti')
        axs[0, 1].set_title("Smrti")

        #graf pro uzdraveni
        axs[1, 0].plot(range(0, self.dict_of_new_healthy.keys().__len__()), self.dict_of_new_healthy.values())
        axs[1, 0].set(xlabel='Dny od pocatku', ylabel='Uzdraveni')
        axs[1, 0].set_title("Uzdraveni")

        #graf koeficientu
        axs[1, 1].plot(range(0, self.rt_dict.__len__()), self.rt_dict.values())
        axs[1, 1].set(xlabel='Dny od pocatku', ylabel='Prirustek za 8 dnu pred tim')
        axs[1, 1].set_title("Jak rychle to roste")

        #graf uspechu leceni
        axs[2, 0].plot(self.uspech_leceni.keys(), self.uspech_leceni.values())
        axs[2, 0].set(xlabel='Mesic', ylabel='Uzdraveni/Nove pripady')
        axs[2, 0].set_title("Uspech leceni")

        #info navic (v jaky den zenrelo nejvic atd.)
        axs[2, 1].text(0.1, 0.8, 'Nejvice tragicky den: ' + self.tragicky_den)
        axs[2, 1].text(0.1, 0.7, 'Zemrelo: ' + str(self.max_smrti))
        axs[2, 1].text(0.1, 0.5, 'Nejvyssi koef. prirustku: ' + str(self.max_rt))

        #ukazujeme to vsechno
        plt.show()
