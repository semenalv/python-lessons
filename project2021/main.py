# This is a sample Python script.

# Press Shift+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.

from csvReader import *
from statistics import *

import numpy as np
import matplotlib.pyplot as plt

def init_statistics():
    #vytvorime statistiku
    stat = statistics()
    #provedeme potrebne veci
    stat.init_reader()


# Spustime to vsechno
if __name__ == '__main__':
    init_statistics()
